#!/usr/bin/env sh

while [ ! -f /run/nologin ]; do
  sleep 1
done

/usr/bin/rm -f /run/nologin
