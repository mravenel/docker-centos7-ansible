FROM centos:7

# Enable systemd
# https://hub.docker.com/_/centos/

ENV container docker

RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
systemd-tmpfiles-setup.service ] || rm -f $i; done); \
rm -f /lib/systemd/system/multi-user.target.wants/*;\
rm -f /etc/systemd/system/*.wants/*;\
rm -f /lib/systemd/system/local-fs.target.wants/*; \
rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
rm -f /lib/systemd/system/basic.target.wants/*;\
rm -f /lib/systemd/system/anaconda.target.wants/*;

# references:
# https://raw.githubusercontent.com/geerlingguy/docker-centos7-ansible/master/Dockerfile
RUN yum makecache fast \
 && yum -y install deltarpm initscripts \
 && yum -y update \
 && yum -y install \
   ansible \
   openssh-server \
   openssl \
   sudo \
   which \
 && yum clean all \
 && systemctl enable sshd

COPY nologin.sh /usr/local/bin/nologin.sh

COPY nologin.service /etc/systemd/system/nologin.service
RUN systemctl enable nologin.service

RUN chmod +x /usr/local/bin/nologin.sh

RUN adduser ansible -p $(openssl passwd -1 password) -G wheel

EXPOSE 22

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/usr/lib/systemd/systemd"]